# Simple Kubernetes App

## Prérequis

- Compte AWS
- AWS CLI installé
- Kubectl installé
- Un cluster EKS existant

## Description

Ce projet contient les fichiers YAML nécessaires pour déployer une application en utilisant le modèle de déploiement Blue-Green sur Kubernetes.

## Structure des fichiers

- `blue-deployment.yaml`: Déploie l'application dans l'environnement "blue".

- `green-deployment.yaml`: Déploie l'application dans l'environnement "green".

- `service.yml`: Expose le service de l'application.

## Configuration AWS CLI

```bash
aws configure
```

## Mise en place du déploiement

1. Mise à jour du fichier Kubeconfig pour EKS

    ```bash
    aws eks update-kubeconfig --region us-east-1 --name bdi-simple-app-eks-cluster
    ```

    - **Commandes utilitaires**

        ```bash
        # Affichage des pods
        kubectl get pods

        # Affichage des noeuds
        kubectl get nodes

        # Affichage des namespaces
        kubectl get ns
        ```

1. Création du namespace

    ```bash
    kubectl create namespace blue-green-deployment

    kubectl config set-context --current --namespace=blue-green-deployment
    # Context "arn:aws:eks:eu-west-3:767397969401:cluster/bdi-simple-app-eks-cluster" modified.

    kubectl get ns
    # blue-green-deployment   Active   39s
    # default                 Active   30m
    # kube-node-lease         Active   30m
    # kube-public             Active   30m
    # kube-system             Active   30m
    ```


2. Application du fichier blue-deployment.yaml pour créer le déploiement "blue"
    ```bash
    kubectl apply -f blue-deployment.yaml
    # deployment.apps/simple-webapp-blue created
    ```

3. Affichage des informations sur le déploiement

    ```bash
    kubectl get deployment
    # NAME           READY   UP-TO-DATE   AVAILABLE   AGE
    # simple-webapp-blue   3/3     3            3           79s
    ```

4. Application du fichier blue-deployment.yaml pour créer le déploiement "blue"

    ```bash
    kubectl apply -f green-deployment.yaml
    # deployment.apps/simple-webapp-green created
    ```

5. Affichage des informations sur le déploiement

    ```bash
    kubectl get deployment

    # NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
    # simple-webapp-blue    0/3     3            0           70s
    # simple-webapp-green   0/3     3            0           20s
    ```

6. Lancement du service

    ```bash
    kubectl apply -f service.yaml
    # service/simple-webapp-service created
    ```

7. Affichage des informations sur le service

    ```bash
    kubectl get service

    # NAME                    TYPE           CLUSTER-IP       EXTERNAL-IP                                                             PORT(S)          AGE
    # simple-webapp-service   LoadBalancer   172.20.228.242   a2f20d275e5de4e42b6ef3dbec38da61-78243555.eu-west-3.elb.amazonaws.com   8080:30000/TCP   17s
    ```

## Supprimer les déploiements

```bash
kubectl get deployments
# NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
# simple-webapp-blue    3/3     3            3           12m
# simple-webapp-green   3/3     3            3           3m16s

kubectl delete deployment simple-webapp-blue
# deployment.apps "simple-webapp-blue" deleted

kubectl delete deployment simple-webapp-green
# deployment.apps "simple-webapp-green" deleted

kubectl get deployments
# No resources found in blue-green-deployment namespace.

kubectl delete service simple-webapp-service
# service "simple-webapp-service" deleted

kubectl get service
# No resources found in blue-green-deployment namespace.
```
